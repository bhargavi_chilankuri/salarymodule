import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label } from 'ng2-charts';
import { DataService } from '../data.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor( private router: Router, public _dataService: DataService) { }
  records = [];
  income = 0;
  expenses = 0;
  savings = 0;

  ngOnInit() {
    this.records = this._dataService.fetchRecords();

    for(var record of this.records) {
      if(record.type === "income"){
        this.income += record.amount;
      }

      else if(record.type === "expenses"){
        this.expenses += record.amount;
      }
     
    }
    console.log("total income is", this.income);
    console.log("total expenses is", this.expenses);
    this.savings = this.income - this.expenses;
    this.doughnutChartData= [
      [this.income,this.expenses, this.savings]
    ];
  }


  /** 
   * @discription: doughnutChart labels and data
   * @author: surekha **/

  doughnutChartLabels: Label[] = ['Income', 'Expenses', 'Savings'];
  doughnutChartData: MultiDataSet = [
    [this.income,this.expenses, this.savings]
  ];
  doughnutChartType: ChartType = 'doughnut';

  /** 
   * @description: add method for expensesform
   * @author: surekha **/

  addExpensesFunction(event) {
    this.router.navigate(['/expenseformpage'])
    .catch(console.error);
  }

  /** 
   * @description: add method for incomeForm
   * @author: surekha **/
  
  addIncomeFunction(event) {
    this.router.navigate(['/incomeformpage'])
    .catch(console.error);
  }

}
