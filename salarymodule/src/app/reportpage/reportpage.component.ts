import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';



@Component({
  selector: 'app-reportpage',
  templateUrl: './reportpage.component.html',
  styleUrls: ['./reportpage.component.scss']
})
export class ReportpageComponent implements OnInit {
  records = [];
  constructor(public _dataService: DataService) { }

  ngOnInit() {
    this.records = this._dataService.fetchRecords();
  }

}