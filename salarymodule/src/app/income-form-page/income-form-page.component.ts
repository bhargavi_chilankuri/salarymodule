import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import{BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import { DataService } from '../data.service';

@Component({
  selector: 'app-income-form-page',
  templateUrl: './income-form-page.component.html',
  styleUrls: ['./income-form-page.component.scss']
})
export class IncomeFormPageComponent implements OnInit {
  group:FormGroup;

  /** @description it shows an Error
   * @author jasmine
   */
  errorMessage='';

  /** @description added bsDatepicker to pick date 
   * @author jasmine
   */
  BsdatePickerConfig: Partial<BsDatepickerConfig>;
  
  constructor(private _dataService:  DataService) { }
  users = [];


/** @description to validate the input type of the value
       * @author jasmine
       */

  ngOnInit() {
    this.group=new FormGroup({
      'amount': new FormControl('',[Validators.required]),
      'source' : new FormControl('',[Validators.required]),
      'date':new FormControl('',[Validators.required]),
      'recivedBy' : new FormControl('',[Validators.required]),
      })
      
  }
/**@description reset button is used to get in initial page
 * @author jasmine
 */

  reset(){
        this.group.reset();
      
    }
  
  /** @description to get error message
       * @author jasmine
       */
      
      addForm(){
        if(this.group.valid){
          let value = this.group.value;
          value['type'] = 'income';
          this._dataService.addRecord(value);
          this.group.reset();
        }  
      }
    }
  
  



